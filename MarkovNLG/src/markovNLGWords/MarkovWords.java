package markovNLGWords;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class MarkovWords
{
    private class similarities
    {
        private char c;
        private int count;
         
        public similarities(char a, int b)
        {
            c = a;
            count = b;
        }
         
        public char getC()
        {
            return c;
        }
         
        public int getCount()
        {
            return count;
        }
         
        public void setCount(int a)
        {
            count = a;
        }
         
        public void increaseCount()
        {
            count++;
        }
    }
 
    public List<similarities> entries;
    public List<String> axies;
    private double[][] matrix;
 
    public MarkovWords()
    {
        entries = new ArrayList();
        axies = new ArrayList();
    }
 
    public void getSimilarity(List<String> s)
    {
        String temp = new String();
        for (int i = 0; i < s.size(); i++)
        {
            if (temp.length() > 0)
            {
                temp = temp + " " + s.get(i);
            }
            else temp = s.get(i);
 
        }
        setupAxis(temp);
        compareWordsAgainstAxis(temp);
 
        display();
 
        for (int i = 0; i < matrix.length; i++)
        {
            double line = getLineValue(i);
            for (int j = 0; j < matrix[i].length; j++){
                if(matrix[i][j]==0.0) continue;
                double t = matrix[i][j];
                matrix[i][j] = t / line;
            }
        }
 
        display();
    }


    private void compareWordsAgainstAxis(String s){
        matrix = new double[axies.size()][axies.size()];
        String comparison = "";
        String test = s;
        String[] words=s.split(" ");

        for (int h = 1; h < words.length; h++){
            for(int j = 1; j < axies.size(); j++){
	            for (int k = 0; k < axies.size(); k++) {
					if(words[h-1].equals(axies.get(j)) && words[h].equals(axies.get(k))){
	            		matrix[j][k]++;
	            	}
	            }
            }
        }
    }

    public void setupAxis(String temp){
    	String[] words=temp.split(" ");
    	
        for (int j = 0; j < words.length; j++){
            if(!axies.contains(words[j])){
            	axies.add(words[j]);
            }
        }
    }
    
 
    private double getTotalValue()
    {
        double sum = 0;
        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[i].length; j++)
            {
                double t = matrix[i][j];
                sum = sum + t;
            }
        }
        return sum;
    }
 
    private double getLineValue(int line)
    {
        double sum = 0;
        for (int i = 0; i < matrix[line].length; i++)
        {
            double t = matrix[line][i];
            sum = sum + t;
        }
        return sum;
    }
 
    private void display()
    {
        System.out.println("Sum of matrix is "+getTotalValue());
        System.out.println("Sum of line 1 = "+getLineValue(3));
 
        System.out.print("  ");
        for (int j = 0; j < axies.size(); j++)
        {
            System.out.print(axies.get(j)+"\t");
        }
        System.out.println();
        for (int i = 0; i < matrix.length; i++)
        {
            System.out.print(axies.get(i)+" ");
            for (int j = 0; j < matrix[i].length; j++)
            {
            	Double num=matrix[i][j];
                String numm= num.toString();
                if(numm.length()>4){System.out.print(numm.substring(0, 5)+"\t");}
                else{System.out.print(numm+"\t");}
            }
            System.out.println();
        }
    }
    private void displayLine(int lineNumber)
    {
        System.out.print("Sum of matrix is "+getTotalValue()+"| ");
        System.out.println("Sum of line 1 = "+getLineValue(3));
 
        System.out.print("\t");
        for (int j = 0; j < axies.size(); j++)
        {
            System.out.print(axies.get(j)+"\t");
        }
        System.out.println();
        System.out.print(axies.get(lineNumber)+"\t");
        for (int j = 0; j < matrix[lineNumber].length; j++)
        {
        	Double num=matrix[lineNumber][j];
            String numm= num.toString();
            if(numm.length()>4){System.out.print(numm.substring(0, 5)+"\t");}
            else{System.out.print(numm+"\t");}
        }
        System.out.println("\n");
    }
    private void displayLineWithWord(String word){
        int lineNumber=axies.indexOf(word);
    	System.out.print("\t");
        for (int j = 0; j < axies.size(); j++)
        {
            System.out.print(axies.get(j)+"\t");
        }
        System.out.println();
        System.out.print(axies.get(lineNumber)+"\t");
        for (int j = 0; j < matrix[lineNumber].length; j++)
        {
        	Double num=matrix[lineNumber][j];
            String numm= num.toString();
            if(numm.length()>4){System.out.print(numm.substring(0, 5)+"\t");}
            else{System.out.print(numm+"\t");}
        }
        System.out.println();
    }
    private int getMaxLocation(int row){
    	double maxLocation=-1.0;
    	int place=-1;
    	boolean maxIsMultiple=false;
    	double test;
    	List<Integer> possible=new ArrayList<Integer>();
    	for(int i=0;i<matrix.length-1;i++){
    		System.out.print(" "+matrix[row][i]+" ");
    		test=matrix[row][i];
    		if(matrix[row][i]>maxLocation){
    			possible.clear();
    			possible.add(i);
    			place=i;
    			maxLocation=matrix[row][i];
    		}else if(matrix[row][i]==maxLocation){
    			possible.add(i);
    		}
    	}
    	if(possible.size()>1){
    		int ind=ThreadLocalRandom.current().nextInt(0, possible.size());
    		return possible.get(ind);
    	}else{
    	return place;
    	}
    }
    private int getMaxWordLocation(int row){
    	double maxLocation=-1.0;
    	int place=-1;
    	boolean maxIsMultiple=false;
    	List<Integer> possible=new ArrayList<Integer>();
    	displayLine(row);
    	for(int i=0;i<matrix.length;i++){
//    		System.out.print(" "+matrix[row][i]+" ");
    		if(matrix[row][i]>maxLocation){
    			possible.clear();
    			possible.add(i);
    			place=i;
                /*blahalerlhlah*/
    			maxLocation=matrix[row][i];
    		}else if(matrix[row][i]==maxLocation){
    			possible.add(i);
    		}
    	}
    	if(possible.size()>1){
    		int ind=ThreadLocalRandom.current().nextInt(0, possible.size());
    		return possible.get(ind);
    	}else{
    	return place;
    	}
    }
    public String getNextWord(String seed){
    	String nWord=seed;
    	String[] word=seed.split(" ");
    	if (word.length>1){
    		System.out.println("upss");
    		System.out.print("next word:");
    		for(int i=0;i<axies.size();i++){
    			if(axies.get(i).equalsIgnoreCase(seed)){
//    				System.out.println(axies.get(getMaxWordLocation(i)));
    				System.out.println("max word location="+getMaxWordLocation(i));
    				return axies.get(getMaxWordLocation(i));
    			}
    		}
    	}else if(word.length==1){
    		for(int i=0;i<axies.size();i++){
        		if(axies.get(i).equals(word[0])){
        			return axies.get(getMaxWordLocation(i));
        		}        		
        	}
    		System.out.println("nope");
    	}else{
    		System.out.println("shit2");
    	}
    	
    	
    	
    	return nWord;
    }
}