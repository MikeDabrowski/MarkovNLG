/**
 * 
 */
package mainApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Ahti
 *
 */
public class DBHandler {
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/javabase?characterEncoding=utf8";
	//  Database credentials
	static final String USER = "Javabase";
	static final String PASS = "Javabase";

	public static void executeQuery(String query,boolean msg){
		Connection conn = null;
		Statement stmt = null;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//STEP 3: Open a connection
			if(msg)System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			if(msg)System.out.println("Connected database successfully...");
  
			//STEP 4: Execute a query
			if(msg)System.out.println("Executing query");
			stmt = conn.createStatement();
  
			//String sql = "CREATE TABLE cz_mowy (id INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY, cz_mowy VARCHAR(30))"; 

			stmt.executeUpdate(query);
			if(msg)System.out.println("Query executed");
		}
		catch(SQLException se){
			//Handle errors for JDBC
		    se.printStackTrace();
		}catch(Exception e){
		    //Handle errors for Class.forName
		    e.printStackTrace();
		}finally{
		    //finally block used to close resources
		    try{
		    	if(stmt!=null)
		    	conn.close();
		    }catch(SQLException se){
		    }// do nothing
		    try{
		    	if(conn!=null)
		        conn.close();
		    }catch(SQLException se){
		    	se.printStackTrace();
		    }//end finally try
		}//end try
	}
	public static void checkConn(){
		Connection conn = null;
		Statement stmt = null;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//STEP 3: Open a connection
			System.out.println("Connecting to a selected database using jdbc:mysql://dabrowski.tk/javabase "+USER+" "+PASS+" ...");
			conn = DriverManager.getConnection("jdbc:mysql://dabrowski.tk/javabase", USER, PASS);
			System.out.println("Connected database successfully...");
  
		}catch(SQLException se){
			//Handle errors for JDBC
		    se.printStackTrace();
		}catch(Exception e){
		    //Handle errors for Class.forName
		    e.printStackTrace();
		}finally{
		    //finally block used to close resources
		    try{
		    	if(stmt!=null)
		    	conn.close();
		    }catch(SQLException se){
		    }// do nothing
		    try{
		    	if(conn!=null)
		        conn.close();
		    }catch(SQLException se){
		    	se.printStackTrace();
		    }//end finally try
		}//end try
		conn = null;
		stmt = null;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//STEP 3: Open a connection
			System.out.println("Connecting to a selected database using "+DB_URL+" "+USER+" "+PASS+" ...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
  
		}catch(SQLException se){
			//Handle errors for JDBC
		    se.printStackTrace();
		}catch(Exception e){
		    //Handle errors for Class.forName
		    e.printStackTrace();
		}finally{
		    //finally block used to close resources
		    try{
		    	if(stmt!=null)
		    	conn.close();
		    }catch(SQLException se){
		    }// do nothing
		    try{
		    	if(conn!=null)
		        conn.close();
		    }catch(SQLException se){
		    	se.printStackTrace();
		    }//end finally try
		}//end try
	}
	public static String selectQuery(String query){
		String output=null;
		query="SELECT id, cz_mowy FROM cz_mowy";
		Connection conn = null;
		Statement stmt = null;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//STEP 3: Open a connection
			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");
  
			//STEP 4: Execute a query
			System.out.println("Executing query");
			stmt = conn.createStatement();
			stmt.executeUpdate(query);
			ResultSet rs = stmt.executeQuery(query);
			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				int id = rs.getInt("id");
				System.out.print("ID: " + id);
			}
			rs.close();
			System.out.println("Query executed");
		}
		catch(SQLException se){
			//Handle errors for JDBC
		    se.printStackTrace();
		}catch(Exception e){
		    //Handle errors for Class.forName
		    e.printStackTrace();
		}finally{
		    //finally block used to close resources
		    try{
		    	if(stmt!=null)
		    	conn.close();
		    }catch(SQLException se){
		    }// do nothing
		    try{
		    	if(conn!=null)
		        conn.close();
		    }catch(SQLException se){
		    	se.printStackTrace();
		    }//end finally try
		}//end try
		return output;
	}
	public static void showDatabase(){
		
	}
	public static String serializeDatabase(){
		return "TODO";
	}
	private boolean validateQuery(String query){
		
		
		return false;
	}
}
