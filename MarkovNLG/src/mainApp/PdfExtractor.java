/**
 * 
 */
package mainApp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;


/**
 * @author mike2
 *
 */
public class PdfExtractor {
	public ArrayList<String> words = new ArrayList<String>();
	public LinkedHashMap<String, String> czMowy= new LinkedHashMap<String, String>();
	public LinkedHashMap<String, String> rodzaje= new LinkedHashMap<String, String>();
	public ArrayList<String> queries = new ArrayList<String>();
	public LinkedHashMap<String, String> zagadnienia=new LinkedHashMap<>();
	public ArrayList<String> alphabet = new ArrayList<String>();
	public PdfExtractor(){
		prepAlphabet();
		prepCzMowy();
		prepWords();
		prepRodz();
	}
	public void prepQueries(boolean msg){
		File file=new File("pdf/SJP-lite-test.txt");
		File output=new File("pdf/out.txt");
		try {
			PrintWriter writer = new PrintWriter(output,"UTF-8");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String line;
			String prevLetter="z";
			while((line = br.readLine()) != null){ 
				StringBuilder q=new StringBuilder();
				//line=br.readLine();
				ArrayList<String> lineAsList = new ArrayList<String>(Arrays.asList(line.toString().split(" ")));
				
				if(lineAsList.size()>=4 && words.contains(lineAsList.get(0))){
					boolean czyCMna1=czMowy.containsKey(lineAsList.get(1));
					boolean czyCMna2=czMowy.containsKey(lineAsList.get(2));
					boolean czyRna2=rodzaje.containsKey(lineAsList.get(2));
					boolean czyRna3=rodzaje.containsKey(lineAsList.get(3));
					if(czyCMna1){
						if(czyRna2){
							q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(1))+"', '"+rodzaje.get(lineAsList.get(2))+"')");
						}else{
							q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(1))+"', '')");
						}
					}else if(czyCMna2){
						if(czyRna3){
							q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(2))+"', '"+rodzaje.get(lineAsList.get(3))+"')");
						}else{
							q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(2))+"', '')");
						}
					}
				}else if(lineAsList.size()==3){
					boolean czyCMna1=czMowy.containsKey(lineAsList.get(1));
					boolean czyRna1=rodzaje.containsKey(lineAsList.get(1));
					boolean czyRna2=rodzaje.containsKey(lineAsList.get(2));
					if(czyCMna1){
						if(czyRna2){
							q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(1))+"', '"+rodzaje.get(lineAsList.get(2))+"')");
						}else{
							q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(1))+"', '')");
						}
					}else if(czyRna1){
							q.append("('"+lineAsList.get(0)+"', '', '"+czMowy.get(lineAsList.get(1))+"')");
					}
					
				}else if(lineAsList.size()==2){
					if(czMowy.containsKey(lineAsList.get(1))){
						q.append("('"+lineAsList.get(0)+"', '"+czMowy.get(lineAsList.get(1))+"', '')");
					}else if(rodzaje.containsKey(lineAsList.get(1))){
						q.append("('"+lineAsList.get(0)+"', '', '"+rodzaje.get(lineAsList.get(1))+"')");
					}
				}
				
				
				if(q.length()>0){
					String currentLetter=Character.toString(lineAsList.get(0).charAt(0));
					int placePrev=alphabet.indexOf(prevLetter);
					int placeCurr=alphabet.indexOf(currentLetter);
					int dist=placeCurr-placePrev;
					if(dist==0){
						writer.println(q.toString()+",");
					}else if(dist==1 || dist == 2/* || dist > 2*/){
						prevLetter=Character.toString(lineAsList.get(0).charAt(0));
						System.out.println(prevLetter);
						writer.println(q.toString()+",");
					}
					//prevLetter=currentLetter;
				}
			}
			writer.close();
			System.out.println("DOOONE");
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
	    } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void putInDatabase(){
		DBHandler dbhandler = new DBHandler();
		File file=new File("pdf/out-query.txt");
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String line2;
			while((line2 = br.readLine()) != null){ 
				dbhandler.executeQuery(line2, false);
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	public void emergencyFix(){
		File file=new File("pdf/out.txt");
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
//			PrintWriter writer = new PrintWriter(output,"UTF-8");
			String line;
			while((line = br.readLine()) != null){ 
				ArrayList<String> lineAsList = new ArrayList<String>(Arrays.asList(line.toString().split(",")));
				System.out.println(lineAsList.toString());
			}
//			writer.println(line);
//			writer.close();
		}catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		
	}
	public void fixReps(){
		File file=new File("pdf/out.txt");
		File output=new File("pdf/out-query.txt");
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			PrintWriter writer = new PrintWriter(output,"UTF-8");
			String line=br.readLine();
			String line2;
			int counter=0;
			while((line2 = br.readLine()) != null){ 
				if(line.equals(line2)){line2=br.readLine();}
				if(counter>=500 && (line.charAt(line.length()-1)==',' && line.charAt(line.length()-2)==')')){
					line=line.substring(0,line.length()-1);
					writer.println(line);
					counter=0;
				}else writer.print(line);
				line=line2;
				counter++;
			}
			writer.println(line);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		
	}
	public void txtToQuery(){
		File file=new File("pdf/out.txt");
		File output=new File("pdf/out-query.txt");
		String queryStart="INSERT INTO `slowa` (`slowo`, `ID_cz_mowy`, `ID_rodzaj`) VALUES ";
		
		try {
			PrintWriter writer = new PrintWriter(output,"UTF-8");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String line;
			while((line = br.readLine()) != null){ 
				StringBuilder q=new StringBuilder();
				q.append(queryStart);
//				line=br.readLine();
				q.append(line+";");
				writer.println(q.toString());
			}
			writer.close();
			System.out.println("DOOONE");
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
	    } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void txtToQueryPlusFixReps(){
		File file=new File("pdf/out.txt");
		File zagFile=new File("pdf/zagadnienia.txt");
		File output=new File("pdf/out-query.txt");
		String queryStart="INSERT INTO `slowa` (`slowo`, `ID_cz_mowy`, `ID_rodzaj`) VALUES ";
		try {
			//file to HashMap			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String line;
			while((line = br.readLine()) != null){
				zagadnienia.put(getKey(line), line);
			}
			System.out.println("Imported to hashmap");
			
			//zapisz do pliku bez powtórzeń
			PrintWriter writer = new PrintWriter(zagFile,"UTF-8");
			for (String klucz : zagadnienia.keySet()) {
				writer.println(zagadnienia.get(klucz));
			}
			writer.close();	
			System.out.println("saved hashmap to file");		
			
			//hashmap to query
			StringBuilder q=new StringBuilder();
			q.append(queryStart);
			PrintWriter writer2 = new PrintWriter(output,"UTF-8");
			int counter=0;
			for (String klucz : zagadnienia.keySet()) {
				q.append(zagadnienia.get(klucz));
				if(counter>500){
					String longline=q.toString();
					longline=longline.substring(0, longline.length()-1);
					writer2.println(longline+";");
					q.setLength(0);
					q.append(queryStart);
					counter=0;
				}				
				counter++;
			}
			writer2.close();
			System.out.println("saved queries to file");
			
			System.out.println("DOOONE");
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
	    } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void prepWords(){
		File wordsFile = new File("pdf/slowa.txt");
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(wordsFile), "UTF-8"));
			String nextWord;
			while ((nextWord = br.readLine()) != null) {
			     if(checkIfWord(nextWord)){
			    	 words.add(nextWord);
			   	 }
			}
			System.out.println("done");
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	private String getKey(String line){
		String key=Arrays.asList(line.toString().split(", ")).get(0);
		key=key.replace("(", "");
		key=key.replace("'", "");
		return key;
	}
	private boolean checkIfWord(String word){
		String[] split=word.split(" ");
		if(split.length>1){return false;}
		else{return true;}
	}
	private void prepCzMowy(){
		czMowy.put("rz.", "rzeczownik");
		czMowy.put("cz.", "czasownik");
		czMowy.put("zaim.", "zaimek");
		czMowy.put("przym.", "przymiotnik");
		czMowy.put("licz.", "liczebnik");
		czMowy.put("przysł.", "przysłówek");
		czMowy.put("mod.", "modulant");
		czMowy.put("przyim.", "przyimek");
		czMowy.put("spój.", "spójnik");
		czMowy.put("relat", "relator");
		czMowy.put("wykrz.", "wykrzyknik");
		czMowy.put("wykrz,", "wykrzyknik");
		czMowy.put("dop.", "dopowiedzenia");
		czMowy.put("oper. met.", "operator metatekstowy");
	}
	private void prepRodz(){
		rodzaje.put("n.", "nijaki");
		rodzaje.put("n","nijaki");
		rodzaje.put("m.","męski");
		rodzaje.put("m","męski");
		rodzaje.put("ż.","żeński");
		rodzaje.put("ż","żeński");
		rodzaje.put("mos", "męskoosobowy");
		rodzaje.put("nmos", "niemęskoosobowy");
	}
	public void prepAlphabet(){
		String a="a,ą,b,c,ć,d,e,ę,f,g,h,i,j,k,l,ł,m,n,ń,o,ó,p,q,r,s,ś,t,u,v,w,x,y,z,ż,ź";
		String[] b=a.split(",");
		alphabet.addAll(Arrays.asList(b));
	}
}
