package mainApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import mainApp.DBHandler;


public class MainApp {
	public static ArrayList<String> words = new ArrayList<String>();
	public static LinkedHashMap<String, String> czMowy= new LinkedHashMap<String, String>();
	public static LinkedHashMap<String, String> rodzaje= new LinkedHashMap<String, String>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		PdfExtractor p=new PdfExtractor();
//		System.out.println(p.alphabet.indexOf(Character.toString('d')));
//		p.prepQueries(false);
//		p.fixReps();
//		p.emergencyFix();
//		p.txtToQueryPlusFixReps();
		p.putInDatabase();
		
		
		
	}
	private static void spr(String t){
		words.add("abnegacja");
		czMowy.put("rz.", "rzeczownik");
		rodzaje.put("ż", "żeński");
		
		ArrayList<String> lineAsList= new ArrayList<String>(Arrays.asList(t.split(" ")));
		boolean czyCMna1=czMowy.containsKey(lineAsList.get(1));
		boolean czyCMna2=czMowy.containsKey(lineAsList.get(2));
		boolean czyRna2=rodzaje.containsKey(lineAsList.get(2));
		boolean czyRna3=rodzaje.containsKey(lineAsList.get(3));
		if(lineAsList.size()>=3 && words.contains(lineAsList.get(0)) && ((czyCMna1 && czyRna2)||(czyCMna2 && czyRna3)) ){
		
			System.out.println(lineAsList.toString());
		}
	}
}
