żyzny przym. 

żywy przym. Ia, -wi, -wszy 1. 'taki, który żyje; żyjący': Żywe istoty, or­
ganizmy. 2. 'energiczny, ruchliwy, pełen życia': Żywa mimika. Żywy
dochód. Żywy intelekt, charakter. Żywe dziecko. 3. 'intensywny, wyra­
zisty': Żywe barwy. Żywe spojrzenie. Żywe uczucie, wzruszenie. Żywa
pamięć. 4. 'o stylu, fabule, akcji utworu literackiego: wartki, pełen wy­
razu': Żywy wątek. Żywa kompozycja. Żywe dialogi. Żywe opowiada­
nie. 5. 'autentyczny, rzeczywisty, prawdziwy': Żywa rzeczywistość. Ży­
we zainteresowanie.
zob. do żywego; handel żywym towarem; inwentarz żywy; kląć w żywy
kamień; nie ma żywej duszy [żywego ducha]; śmiać się [roześmiać się
i syn.] {komuś} w twarz [i in.]
żywy [chodzący] kościotrup 'o kimś bardzo chudym, źle wyglądającym': Z
czego ona się chce odchudzić? Przecież to chodzący kościotrup.
żywy trup pot. 'człowiek wyniszczony chorobą lub używkami, bardzo wy­
cieńczony fizycznie lub psychicznie, bardzo zmęczony, blady': Wyglądać
jak żywy trup. Snujesz się jak żywy trup.
żyzny przym. Ia, żyźniejszy 1. 'odnoszący się do gleby: mający takie właści­
wości biologiczne, fizyczne i chemiczne, które zapewniają roślinom up­
rawnym optymalne warunki rozwoju, gwarantujący otrzymanie wyso­
kich plonów; urodzajny, płodny': Żyzne podłoże. Żyzne grunty, pola. 2.
'taki, którego gleby są urodzajne': Żyzne obszary ziemi, okolice. Żyzna
część kontynentu.
